#!/usr/bin/env python3
 # -*- coding:utf-8 -*-

# esta función crea una lista con todos los datos del archivo, una lista por pelicula
class List():

    def __init__(self):
        lista = []
        archivo = open("IMDb movies.csv")
        for i, linea in enumerate(archivo):
            linea = linea.split(",")
            if i > 0:
                lista.append([])
                j = 0
                # hasta recorrer la línea
                while j != (len(linea) - 1):
                    mas = []
                    if j != (len(linea) - 1):
                        try:
                            if linea[j + 1][0] == " ":
                                while linea[j + 1][0] == " ":
                                    mas.append(linea[j])
                                    j += 1
                                mas.append(linea[j])
                                j += 1
                            else:
                                mas.append(linea[j])
                                j += 1
                            lista[i - 1].append(mas)
                            mas = []
                        except IndexError:
                            lista[i - 1].append([linea[j]])
                            j += 1
                    else:
                        lista.append(linea[j])
        archivo.close()
        self.lista = lista
