#!/usr/bin/env python3
 # -*- coding:utf-8 -*-

"""
    IDEA: tomar i = 0 en Listar, y nombrar los self de acuerdo a las columnas
    si se puede, se ahorran líneas
"""

class Datos():

    def __init__(self, i):
        self.id = i[0]
        self.titulo = i[1]
        self.tituloriginal = i[2]
        self.ano = i[3]
        self.fecha = i[4]
        self.genero = i[5]
        self.duracion = i[6]
        self.pais = i[7]
        self.idioma = i[8]
        self.director = i[9]
        self.escritor = i[10]
        self.productora = i[11]
        self.elenco = i[12]
        self.descripcion = i[13]
        self.avg_vote = i[14]
        self.votes = i[15]
        self.budget = i[16]
        self.usa_gross_income = i[17]
        self.worlwide_gross_income = i[18]
        self.metascore = i[19]
        self.reviews_from_users = i[20]
        self.reviews_from_critics = i[21]
